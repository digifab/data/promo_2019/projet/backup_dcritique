USE [DCritique]
GO
/****** Object:  Trigger [dbo].[MetaData]    Script Date: 03/12/2019 11:49:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Noah
-- Create date: 
-- Description:	
-- =============================================
/*CREATE OR ALTER TRIGGER [dbo].[MetaData] 
   ON  [dbo].[Personnages] 
   AFTER INSERT,DELETE,UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON
    -- Insert statements for trigger here
	IF COLUMNS_UPDATED() IS NOT NULL
	BEGIN
		INSERT INTO MetaDonnees VALUES (CURRENT_TIMESTAMP,'Nom FICHER','CHEMIN','Source','kesako de quoi passer');
	END
	ELSE IF (SELECT * FROM INSERTED) IS NOT NULL
	BEGIN
		INSERT INTO MetaDonnees VALUES ();
	END
	ELSE IF (SELECT * FROM DELETED) IS NOT NULL
	BEGIN
		INSERT INTO MetaDonnees VALUES ();
	END

END
*/