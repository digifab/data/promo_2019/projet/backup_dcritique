﻿namespace DCritique
{
    partial class DataViz
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title2 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.RaceAdressMail = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.vueDataViz1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dCritiqueDataSet = new DCritique.DCritiqueDataSet();
            this.vueDataViz1TableAdapter = new DCritique.DCritiqueDataSetTableAdapters.VueDataViz1TableAdapter();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.vueDataViz2BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vueDataViz2TableAdapter = new DCritique.DCritiqueDataSetTableAdapters.VueDataViz2TableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.RaceAdressMail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vueDataViz1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCritiqueDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vueDataViz2BindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // RaceAdressMail
            // 
            chartArea1.AxisX.Title = "TestX";
            chartArea1.AxisY.Title = "TestY";
            chartArea1.Name = "ChartArea1";
            this.RaceAdressMail.ChartAreas.Add(chartArea1);
            this.RaceAdressMail.DataSource = this.vueDataViz1BindingSource;
            legend1.Name = "Legend1";
            legend1.Title = "Adresses Mail";
            this.RaceAdressMail.Legends.Add(legend1);
            this.RaceAdressMail.Location = new System.Drawing.Point(12, 12);
            this.RaceAdressMail.Name = "RaceAdressMail";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series1.IsValueShownAsLabel = true;
            series1.IsXValueIndexed = true;
            series1.Legend = "Legend1";
            series1.LegendToolTip = "#TOTAL";
            series1.Name = "Personnages par Adress Mail";
            series1.XValueMember = "adressMail";
            series1.YValueMembers = "nbPersonnage";
            series1.YValuesPerPoint = 2;
            this.RaceAdressMail.Series.Add(series1);
            this.RaceAdressMail.Size = new System.Drawing.Size(511, 538);
            this.RaceAdressMail.TabIndex = 0;
            this.RaceAdressMail.Text = "chart1";
            title1.Name = "Titre";
            title1.Text = "Nombre de Personnages par Adresse Mail";
            this.RaceAdressMail.Titles.Add(title1);
            this.RaceAdressMail.Click += new System.EventHandler(this.RaceAdressMail_Click);
            // 
            // vueDataViz1BindingSource
            // 
            this.vueDataViz1BindingSource.DataMember = "VueDataViz1";
            this.vueDataViz1BindingSource.DataSource = this.dCritiqueDataSet;
            // 
            // dCritiqueDataSet
            // 
            this.dCritiqueDataSet.DataSetName = "DCritiqueDataSet";
            this.dCritiqueDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // vueDataViz1TableAdapter
            // 
            this.vueDataViz1TableAdapter.ClearBeforeFill = true;
            // 
            // chart1
            // 
            chartArea2.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea2);
            this.chart1.DataSource = this.vueDataViz2BindingSource;
            legend2.Name = "Legend1";
            legend2.Title = "Races";
            this.chart1.Legends.Add(legend2);
            this.chart1.Location = new System.Drawing.Point(529, 12);
            this.chart1.Name = "chart1";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series2.IsValueShownAsLabel = true;
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            series2.XValueMember = "nomRace";
            series2.YValueMembers = "nbPersonnage";
            this.chart1.Series.Add(series2);
            this.chart1.Size = new System.Drawing.Size(467, 538);
            this.chart1.TabIndex = 1;
            this.chart1.Text = "chart1";
            title2.Name = "Title1";
            title2.Text = "Nombre de personnage par race";
            this.chart1.Titles.Add(title2);
            // 
            // vueDataViz2BindingSource
            // 
            this.vueDataViz2BindingSource.DataMember = "VueDataViz2";
            this.vueDataViz2BindingSource.DataSource = this.dCritiqueDataSet;
            // 
            // vueDataViz2TableAdapter
            // 
            this.vueDataViz2TableAdapter.ClearBeforeFill = true;
            // 
            // DataViz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 562);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.RaceAdressMail);
            this.Name = "DataViz";
            this.Text = "DataViz";
            this.Load += new System.EventHandler(this.DataViz_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RaceAdressMail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vueDataViz1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dCritiqueDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vueDataViz2BindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart RaceAdressMail;
        private DCritiqueDataSet dCritiqueDataSet;
        private System.Windows.Forms.BindingSource dataVizBindingSource;
        private System.Windows.Forms.BindingSource vueDataViz1BindingSource;
        private DCritiqueDataSetTableAdapters.VueDataViz1TableAdapter vueDataViz1TableAdapter;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.BindingSource vueDataViz2BindingSource;
        private DCritiqueDataSetTableAdapters.VueDataViz2TableAdapter vueDataViz2TableAdapter;
    }
}