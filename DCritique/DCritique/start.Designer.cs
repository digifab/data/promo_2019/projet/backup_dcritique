﻿namespace DCritique
{
    partial class Start
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelMetaData = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelMetaData
            // 
            this.labelMetaData.AutoSize = true;
            this.labelMetaData.Location = new System.Drawing.Point(13, 13);
            this.labelMetaData.Name = "labelMetaData";
            this.labelMetaData.Size = new System.Drawing.Size(0, 13);
            this.labelMetaData.TabIndex = 0;
            // 
            // CréationFeuillePersonnage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelMetaData);
            this.Name = "CréationFeuillePersonnage";
            this.Text = "MetaData";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelMetaData;
    }
}

