﻿using System;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;

namespace DCritique
{
    public partial class Backup : Form
    {
        SqlConnection con = new SqlConnection("Server=Noah_FIQUET\\SQLEXPRESS;Database=DCritique;Trusted_Connection=True;");
        /// <summary>
        /// Initialise les composant
        /// Appel de la Methode Backup()
        /// Utilise un TImer qui après 6H rapelle la Methode Backup()
        /// </summary>
        public Backup()
        {
            InitializeComponent();
            con.Open();
            UpdatePivot();
            BackupDB();
            con.Close();
            Timer timerBackup = new Timer();
            timerBackup.Interval = 21600000;    //6H
            timerBackup.Tick += (s, e) => { UpdatePivot(); BackupDB();};
            timerBackup.Enabled = true;
            timerBackup.Start();
        }
        public void UpdatePivot()
        {
            string query = "TRUNCATE TABLE DataViz;" +
                " INSERT INTO DataViz SELECT  Personnages.nomPersonnage, Mails.adressMail, Univers.nomUnivers, Races.nomRace, CURRENT_TIMESTAMP" +
                " FROM Personnages INNER JOIN" +
                " Races ON Personnages.idRace = Races.idRace INNER JOIN" +
                " Univers ON Races.idUnivers = Univers.idUnivers INNER JOIN" +
                " Mails ON Personnages.idMail = Mails.idMail";
            SqlCommand com = new SqlCommand(query, con);
            com.ExecuteNonQuery();
        }
        /// <summary>
        /// Créer une connexion avec la base de donnée.
        /// Récupère le chemin où seron les Saves.
        /// Créer le nom de la sauvegarde avec la Date et l'heure du jours.
        /// Si elle existe déjà alors la suprime et la recréer puis alimente  le fichier log.
        /// Sinon la créer et alimente  le fichier log.
        /// Si elle n'y arrive pas alimente le fichier log d'un echec.
        /// Enfin lie le fichier log dans la WebForm
        /// </summary>
        public void BackupDB()
        {
            string path = "C:\\Users\\noah\\Desktop\\D-CRITIQUE\\";
            string backup = "backup database DCritique to disk = '" + path + "DCritique_" + DateTime.Today.ToString("D").Replace(" ", "_") + ".bak'";
            StreamWriter log = new StreamWriter(path + "DCritique_Backup_LOG.txt", true); //Permet d'écrire dans le fichier LOG
            SqlCommand com = new SqlCommand(backup, con);
            string filename = "DCritique_" + DateTime.Today.ToString("D").Replace(" ", "_") + ".bak";
            if (File.Exists(path + filename))
            {
                string deleteSave = DateTime.Now.ToString();
                File.Delete(filename);
                com.ExecuteNonQuery();
                log.WriteLine("La sauvegarde " + filename + " Existe déjà, elle a été supprimé le " + deleteSave + " puis remplacé le " + DateTime.Now);

            }
            else
            {
                string startSave = DateTime.Now.ToString();
                com.ExecuteNonQuery();
                log.WriteLine("La sauvegarde " + filename + " a Commencé le " + startSave + " est a réussie le  " + DateTime.Now);
            }
            if (File.Exists(path + filename) == false)
            {
                log.WriteLine("La Sauvegarde " + filename + " a échoué le " + DateTime.Now);
            }
            log.Close();
            labelBackup.Text = File.ReadAllText(path + "DCritique_Backup_LOG.txt");
        }
    }
}
