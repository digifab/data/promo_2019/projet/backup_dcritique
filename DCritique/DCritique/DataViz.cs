﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DCritique
{
    public partial class DataViz : Form
    {
        public DataViz()
        {
            InitializeComponent();
        }

        private void DataViz_Load(object sender, EventArgs e)
        {
            // TODO: cette ligne de code charge les données dans la table 'dCritiqueDataSet.VueDataViz2'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.vueDataViz2TableAdapter.Fill(this.dCritiqueDataSet.VueDataViz2);
            // TODO: cette ligne de code charge les données dans la table 'dCritiqueDataSet.VueDataViz1'. Vous pouvez la déplacer ou la supprimer selon les besoins.
            this.vueDataViz1TableAdapter.Fill(this.dCritiqueDataSet.VueDataViz1);

        }

        private void RaceAdressMail_Click(object sender, EventArgs e)
        {

        }
    }
}
