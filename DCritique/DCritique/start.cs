﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net.Mail;
using ActiveUp.Net.Mail;
using PdfSharp.Pdf;
using System.IO;
using PdfSharp.Pdf.IO;
using PdfSharp.Drawing;
using org.apache.pdfbox.pdmodel;
using org.apache.pdfbox.util;
using System.Windows.Forms;

namespace DCritique
{
    public partial class Start : Form
    {
        // Connexion au serveur lecture du mail et ajout des mots dans un tableau
        SqlConnection con = new SqlConnection("Server=Noah_FIQUET\\SQLEXPRESS;Database=DCritique;Trusted_Connection=True;");
        MailRepository mailRepository = new MailRepository("pop.gmail.com", 993, true, "persodcritique@gmail.com", "Nono19970628");
        System.Net.Mail.SmtpClient SmtpServer = new System.Net.Mail.SmtpClient("smtp.gmail.com");
        static string chemin_mail_a_importer = "";
        static string bodyMail = "";
        static String[] separator = { ",", ";", ":", " ", "\n", "\r" };
        String[] tableauValues = bodyMail.ToLower().Split(separator, StringSplitOptions.RemoveEmptyEntries);
        static string mail;
        public Start()
        {
            InitializeComponent();
            DataViz chart = new DataViz();
            Backup saveDB = new Backup();
            saveDB.Show();
            chart.Show();
            Timer timerMail = new Timer();
            timerMail.Interval = 3000;
            timerMail.Tick += (s, e) => { LireMail(); };
            timerMail.Enabled = true;
            timerMail.Start();

        }
        public void listAllFiles(ref List<String> allFiles, string path, string ext, bool scanDirOk)
        {


            // listFilesCurrDir : Tableau contenant  la liste des fichiers du dossier 'path'
            string[] listFilesCurrDir = Directory.GetFiles(path, ext);

            // On lit le tableau 'listFilesCurrDir' 
            foreach (string rowFile in listFilesCurrDir)
            {
                // Si le fichier n'est pas deja dans la liste 'allFiles'
                if (allFiles.Contains(rowFile) == false)
                {
                    // On ajoute le fichier (du moins son adresse) a 'allFiles'
                    allFiles.Add(rowFile);
                }
            }



        }

        private string ajout_zero(int valeur)
        {

            if (valeur < 10)
                return "0" + valeur.ToString();
            else
                return valeur.ToString();

        }

        private int indexer_repertoire(string repertoire)
        {
            List<string> liste_rep = new List<string>(Directory.EnumerateDirectories(repertoire));
            int index_rep_max = 0;


            foreach (string rep_test in liste_rep)
            {
                string sous_rep = rep_test.Replace(repertoire + "\\", "");
                int int_test;
                if (int.TryParse(sous_rep, out int_test))
                {
                    if (int_test > index_rep_max)
                    {
                        index_rep_max = int_test;
                    }
                }
            }

            return index_rep_max + 1;
        }



        public void LireMail()
        {

            string chermin_depart = @"C:\Users\noah\Desktop\D-CRITIQUE\RECEPMAIL\";
            IEnumerable<ActiveUp.Net.Mail.Message> emailList =  mailRepository.GetUnreadMails("inbox");
            labelMetaData.Text =  emailList.GetEnumerator().ToString();
            // simplement liste mail non lu sinon GetAllMails
            foreach (ActiveUp.Net.Mail.Message email in emailList)
            {
                List<string> liste_metadonnees = new List<string>();

                List<string> liste_ligne_mail = new List<string>();

                liste_ligne_mail.Add("EMAIL : " + email.From.Email);
                liste_ligne_mail.Add("DATE  : " + email.DateString);
                liste_ligne_mail.Add("SUJET :" + email.Subject);
                liste_ligne_mail.Add("MESSAGEID :" + email.MessageId.ToString());

                string texte_mail = email.BodyText.Text;

                bool mode_mail = false;

                if (texte_mail.Trim() == "")
                {
                    mode_mail = true;
                    texte_mail = email.BodyHtml.Text;
                }

                // ajout dans la liste du corps du mail
                nettoyage_et_decoupage_en_ligne(liste_ligne_mail, texte_mail, mode_mail);

                // creation repertoire a importer
                chemin_mail_a_importer = System.IO.Path.Combine(chermin_depart, "traitement");
                int id_message = indexer_repertoire(chemin_mail_a_importer);
                chemin_mail_a_importer = System.IO.Path.Combine(chemin_mail_a_importer, id_message.ToString());
                System.IO.Directory.CreateDirectory(chemin_mail_a_importer);

                DateTime maintenant = DateTime.Now;
                string date_heure = DateTime.Today.Year + "_" + ajout_zero(maintenant.Month) + "_" + ajout_zero(maintenant.Day) + "_" + ajout_zero(maintenant.Hour) + "_" + ajout_zero(maintenant.Minute) + "_" + ajout_zero(maintenant.Second);

                string fichier_mail_a_importer_corps = System.IO.Path.Combine(chemin_mail_a_importer, "mail_num_corps_" + id_message.ToString() + "_" + date_heure + ".txt");
                string fichier_mail_a_importer_joint = System.IO.Path.Combine(chemin_mail_a_importer, "mail_num_joint_" + id_message.ToString() + "_" + date_heure + ".txt");
                string fichier_mail_a_importer_complet = System.IO.Path.Combine(chemin_mail_a_importer, "mail_num_complet_" + id_message.ToString() + "_" + date_heure + ".txt");
                string fichier_metadonnee = System.IO.Path.Combine(chemin_mail_a_importer, "metadonnee_" + id_message.ToString() + "_" + date_heure + ".txt");
                string chemin_repertoire_fichiers_joint = System.IO.Path.Combine(chemin_mail_a_importer, "joint");

                // engegistrement fichier corps
                ecriture_fichier(fichier_mail_a_importer_corps, liste_ligne_mail);

                liste_metadonnees.Add("{");
                liste_metadonnees.Add("Brut_corps : {");
                liste_metadonnees.Add("source : mail ref =" + email.MessageId.ToString() + ",");
                liste_metadonnees.Add("fichier : " + "mail_num_corps_" + id_message.ToString() + "_" + date_heure + ".txt" + ",");
                liste_metadonnees.Add("emplacement  : " + chemin_mail_a_importer);
                liste_metadonnees.Add("date : " + DateTime.Now.ToString());
                liste_metadonnees.Add("},");

                // si plusieurs fichiers joint
                if (email.Attachments.Count > 0)
                {
                    List<string> liste_texte_joint = new List<string>();

                    int i = 1;
                    System.IO.Directory.CreateDirectory(chemin_repertoire_fichiers_joint);
                    foreach (MimePart attachment in email.Attachments)
                    {
                        string fichiers_joint = System.IO.Path.Combine(chemin_repertoire_fichiers_joint, attachment.Filename);

                        attachment.StoreToFile(fichiers_joint); // "C:\\RECEPMAIL_TMP\\" + attachment.Filename);

                        string extention = Path.GetExtension(fichiers_joint);
                        if (extention == ".pdf")
                        {
                            // conversion en texte
                            string fichier_ecriture_txt = fichiers_joint.Replace(".pdf", ".txt");

                            DateTime start = DateTime.Now;

                            using (StreamWriter sw = new StreamWriter(fichier_ecriture_txt))
                            {
                                sw.WriteLine(parseUsingPDFBox(fichiers_joint));
                            }

                            liste_metadonnees.Add("Fichier_joint_pdf_txt" + i.ToString() + " : {");
                            liste_metadonnees.Add("source : mail ref =" + email.MessageId.ToString() + ",");
                            liste_metadonnees.Add("fichier : " + fichier_ecriture_txt + ",");
                            liste_metadonnees.Add("emplacement  : " + chemin_repertoire_fichiers_joint);
                            liste_metadonnees.Add("date : " + DateTime.Now.ToString());
                            liste_metadonnees.Add("},");

                            lecture_fichier(ref liste_texte_joint, fichier_ecriture_txt);

                        }
                        if (extention == ".txt")
                        {
                            lecture_fichier(ref liste_texte_joint, fichiers_joint);
                        }


                        liste_metadonnees.Add("Fichier_joint_" + i.ToString() + " : {");
                        liste_metadonnees.Add("source : mail ref =" + email.MessageId.ToString() + ",");
                        liste_metadonnees.Add("fichier : " + attachment.Filename + ",");
                        liste_metadonnees.Add("emplacement  : " + chemin_repertoire_fichiers_joint);
                        liste_metadonnees.Add("date : " + DateTime.Now.ToString());
                        liste_metadonnees.Add("},");
                    }

                    ecriture_fichier(fichier_mail_a_importer_joint, liste_texte_joint);

                    // recopie fichier joint dans
                    foreach (string l_joint in liste_texte_joint)
                    {
                        liste_ligne_mail.Add(l_joint);
                    }

                    liste_metadonnees.Add("Brut_joint : {");
                    liste_metadonnees.Add("source : mail ref =" + email.MessageId.ToString() + ",");
                    liste_metadonnees.Add("fichier : " + "mail_num_joint_" + id_message.ToString() + "_" + date_heure + ".txt" + ",");
                    liste_metadonnees.Add("emplacement  : " + chemin_mail_a_importer);
                    liste_metadonnees.Add("date : " + DateTime.Now.ToString());
                    liste_metadonnees.Add("},");

                    // analyse si fichier joint text ou pdf
                    // conversion en texte fichier si besoin                     
                }

                ecriture_fichier(fichier_mail_a_importer_complet, liste_ligne_mail);

                liste_metadonnees.Add("Brut_complet : {");
                liste_metadonnees.Add("source : mail ref =" + email.MessageId.ToString() + ",");
                liste_metadonnees.Add("fichier : " + "mail_num_complet_" + id_message.ToString() + "_" + date_heure + ".txt" + ",");
                liste_metadonnees.Add("emplacement  : " + chemin_mail_a_importer);
                liste_metadonnees.Add("date : " + DateTime.Now.ToString());
                liste_metadonnees.Add("}");
                liste_metadonnees.Add("}");
                ecriture_fichier(fichier_metadonnee, liste_metadonnees);
                mail = email.From.Email;
                foreach (string l in liste_ligne_mail)
                {
                    string ligne_minuscule = l.ToLower();
                    ligne_minuscule = ligne_minuscule.Replace("volonté", "volonte");
                    ligne_minuscule = ligne_minuscule.Replace("rapidité", "rapidite");
                    ligne_minuscule = ligne_minuscule.Replace("dextérité", "dexterite");
                    ligne_minuscule = ligne_minuscule.Replace("\"", "");
                    bodyMail = bodyMail + " " + ligne_minuscule;
                }
                tableauValues = bodyMail.ToLower().Split(separator, StringSplitOptions.RemoveEmptyEntries);
                AjoutPersonnage();
            }
        }

        private void ecriture_fichier(string nom_fichier, List<string> list_ligne)
        {
            try
            {
                //Pass the filepath and filename to the StreamWriter Constructor
                StreamWriter sw = new StreamWriter(nom_fichier);
                //Write a line of text

                foreach (string ligne in list_ligne)
                {
                    sw.WriteLine(ligne);
                }

                sw.Close();
            }
            catch (Exception e)
            {
                labelMetaData.Text = e.Message;
            }
            finally
            {
                Console.WriteLine("Executing finally block.");
            }
        }

        private void lecture_fichier(ref List<string> liste_ligne, string nom_fichier)
        // lecture dans une liste de string un fichier
        {

            String line; try
            {
                //Pass the file path and file name to the StreamReader constructor
                StreamReader sr = new StreamReader(nom_fichier);
                //Read the first line of text
                line = sr.ReadLine();
                //Continue to read until you reach end of file
                while (line != null)
                {
                    liste_ligne.Add(line);
                    //write the lie to console window
                    //Console.WriteLine(line);
                    //Read the next line
                    line = sr.ReadLine();
                }
                //close the file
                sr.Close();
                //Console.ReadLine();
            }
            catch (Exception e)
            {
                labelMetaData.Text = e.Message;
            }
            finally
            {
                //Console.WriteLine("Executing finally block.");
            }
        }

        private String nettoyage_recursif_html(String texte_html)
        {

            int premier = texte_html.IndexOf("<");
            int dernier = texte_html.IndexOf(">");


            if (premier >= 0 && dernier > 0)
            {
                texte_html = texte_html.Replace(texte_html.Substring(premier, dernier - premier + 1), "");
                texte_html = nettoyage_recursif_html(texte_html);
            }

            return texte_html;
        }

        private void nettoyage_et_decoupage_en_ligne(List<string> liste_ligne, string texte_mail, bool mode_mail)
        // decoupage de la ligne
        {
            if (mode_mail)
            {
                texte_mail = texte_mail.Replace("&lt;", "<");
                texte_mail = texte_mail.Replace("&gt;", ">");
                texte_mail = texte_mail.Replace("&amp;", "&");
                texte_mail = texte_mail.Replace("&quot;", "\"");
                texte_mail = texte_mail.Replace("&nbsp;", " ");

                texte_mail = texte_mail.Replace("&eacute;", "é");
                texte_mail = texte_mail.Replace("&Eacute;", "É");
                texte_mail = texte_mail.Replace("&egrave;", "è");
                texte_mail = texte_mail.Replace("&ecirc;", "ê");
                texte_mail = texte_mail.Replace("&agrave;", "à");

                texte_mail = texte_mail.Replace("&iuml;", "ï");
                texte_mail = texte_mail.Replace("&ccedil;", "ç");
                texte_mail = texte_mail.Replace("&gt;", ">");
                texte_mail = texte_mail.Replace("&amp;", "&");
                texte_mail = texte_mail.Replace("&ntilde;", "ñ");

                texte_mail = texte_mail.Replace("&AElig;", "Æ");
                texte_mail = texte_mail.Replace("&Eacute;", "É");
                texte_mail = texte_mail.Replace("&egrave;", "è");
                texte_mail = texte_mail.Replace("&ecirc;", "ê");
                texte_mail = texte_mail.Replace(";", "");

                texte_mail = texte_mail.Replace("&euro;", "€");
                texte_mail = texte_mail.Replace("&#8364;", "€");
                texte_mail = texte_mail.Replace("&copy;", "©");
                texte_mail = texte_mail.Replace("&#169;", "©");
                texte_mail = texte_mail.Replace("&reg;", "®");

                texte_mail = texte_mail.Replace("&#174;", "®");
                texte_mail = texte_mail.Replace("&deg;", "°");
                texte_mail = texte_mail.Replace("&#176;", "°");
                texte_mail = texte_mail.Replace("&ordm;", "º");
                texte_mail = texte_mail.Replace("&#171;", "«");

                texte_mail = texte_mail.Replace("&laquo;", "«");
                texte_mail = texte_mail.Replace("&#187;", "»");
                texte_mail = texte_mail.Replace("&raquo;", "»");
                texte_mail = texte_mail.Replace("&micro;", "µ");
                texte_mail = texte_mail.Replace("&#181;", "µ");

                texte_mail = texte_mail.Replace("&para;", "¶");
                texte_mail = texte_mail.Replace("&#182;", "¶");
                texte_mail = texte_mail.Replace("&frac14;", "¼");
                texte_mail = texte_mail.Replace("&#188;", "¼");
                texte_mail = texte_mail.Replace("&#189;", "½");

                texte_mail = texte_mail.Replace("&frac12;", "½");
                texte_mail = texte_mail.Replace("&#190;", "¾");
                texte_mail = texte_mail.Replace("&frac34;", "¾");
                texte_mail = texte_mail.Replace("&#156;", "œ");
            }

            texte_mail = nettoyage_recursif_html(texte_mail);

            string[] tab_ligne;

            texte_mail = texte_mail.Replace("\r", "");
            tab_ligne = texte_mail.Split('\n');

            foreach (string li in tab_ligne)
            {
                liste_ligne.Add(li);
            }
        }

        private static string parseUsingPDFBox(string input)
        {
            PDDocument doc = null;

            try
            {
                doc = PDDocument.load(input);
                PDFTextStripper stripper = new PDFTextStripper();
                return stripper.getText(doc);
            }
            finally
            {
                if (doc != null)
                {
                    doc.close();
                }
            }
        }
        public void AjoutPersonnage()
        {
            try
            {
                //appel des Methodes 
                con.Open();
                mail = SearchMail();
                string univers = SearchUnivers();
                string race = SearchRace();
                string nom = SearchNom();
                string signe = SearchSigne();
                string archetype = SearchArchetype();
                int force = SearchForce();
                int endurance = SearchEndurance();
                int dexterite = SearchDexterite();
                int rapidite = SearchRapidite();
                int volonte = SearchVolonte();
                int perception = SearchPerception();
                int intelligence = SearchIntelligence();
                int charisme = SearchCharisme();
                // verrification que le personnage ne dépasse pas 3 points d'Amelioration
                int total = force + endurance + dexterite + rapidite + volonte + perception + intelligence + charisme;
                if (total >= 4)
                {
                    EnvoieMailError("Votre personnage ne peut pas dépasser 3 points d'améliorations.");
                }
                else if (univers is null || race is null || nom is null)
                {
                    EnvoieMailError("Votre Personnage n'est pas complète\nVoici les élements requis pour la création d'un personnage\nUnivers : (le nom de l'Univers)\nRace : (le nom de la race)\nNom : (le nom du personnage)\n\nIl vous manque plus que le ou les bonus des attributs de votre race entre la Force , l'Endurance, la Dexterité, la Rapidité, la Perception, la Volonté, l'Intelligence,le Charsime\nVous pouvez augmentez un attribut de 2 et un autre de 1\nOu\nAugmentez 3 Attributs de 1");
                }
                else
                {
                    //envoie de des données récupérée
                    string query = "INSERT INTO RecupPersonnages VALUES ('" + mail + "','"
                                                                            + nom + "','"
                                                                            + univers + "','"
                                                                            + race + "','"
                                                                            + archetype + "','"
                                                                            + signe + "',"
                                                                            + force + ","
                                                                            + endurance + ","
                                                                            + dexterite + ","
                                                                            + rapidite + ","
                                                                            + perception + ","
                                                                            + volonte + ","
                                                                            + intelligence + ","
                                                                            + charisme + ");";
                    SqlCommand envoie = new SqlCommand(query, con);
                    envoie.ExecuteNonQuery();
                    CreatePersonnage();
                    con.Close();
                }
            }
            catch (SqlException e)
            {
                labelMetaData.Text = e.Message.Replace(".", "\n");
            }
        }
        public string SearchMail()
        {
            string query = "SELECT adressMail FROM Mails";
            SqlCommand com = new SqlCommand(query, con);
            SqlDataReader reader = com.ExecuteReader();
            while (reader.Read())
            {
                String[] tableauKey = { reader[0].ToString() };
                foreach (string mailKey in tableauKey)
                {
                    if (mailKey == mail)
                    {
                        reader.Close();
                        return mail;
                    }
                }
            }
            reader.Close();
            string queryMail = "INSERT INTO Mails VALUES ('" + mail + "');";
            SqlCommand newMail = new SqlCommand(queryMail, con);
            newMail.ExecuteNonQuery();
            return SearchMail();
        }
        /// <summary>
        /// Selection tout les nom d'Univers et compare aves le tableau des mots du mail et si une valeur correspond la retourne sinon l'ajoute de la BDD et la retourne dans tout les cas la permiers lettre est en Majuscule
        /// </summary>
        /// <returns></returns>
        public string SearchUnivers()
        {
            string query = "SELECT nomUnivers FROM Univers";
            SqlCommand com = new SqlCommand(query, con);
            SqlDataReader reader = com.ExecuteReader();
            while (reader.Read())
            {
                String[] tableauKey = { reader[0].ToString() };
                foreach (string universKey in tableauKey)
                {
                    foreach (string universValue in tableauValues) //Tout les mots du mail sont dans tableauValues. 
                    {
                        if (universKey.ToLower() == universValue)
                        {
                            reader.Close();
                            return char.ToUpper(universValue[0]) + universValue.Substring(1);
                        }
                    }
                }
            }
            for (int i = 0; i < tableauValues.Length; i++)
            {
                if (tableauValues[i] == "univers")
                {
                    string n = null;
                    foreach (char v in tableauValues[i + 1])
                    {
                        if (v == tableauValues[i + 1][0])
                        {
                            n += v.ToString().ToUpper();
                        }
                        else
                        {
                            n += v;
                        }
                    }
                    string queryUnivers = "INSERT INTO Mails VALUES ('" + n + "',null);";
                    SqlCommand newUnivers = new SqlCommand(queryUnivers, con);
                    newUnivers.ExecuteNonQuery();
                    reader.Close();
                    return n;
                }
            }
            reader.Close();
            return null;
        }
        /// <summary>
        ///  Selection tout les nom des Races et compare aves le tableau des mots du mail et si une valeur correspond la retourne sinon l'ajoute de la BDD et la retourne dans tout les cas la permiers lettre est en Majuscule
        /// </summary>
        /// <returns> </returns>
        public string SearchRace()
        {
            string query = "SELECT nomRace FROM Races";
            SqlCommand com = new SqlCommand(query, con);
            SqlDataReader reader = com.ExecuteReader();
            while (reader.Read())
            {
                String[] tableauKey = { reader[0].ToString() };
                foreach (string raceKey in tableauKey)
                {
                    foreach (string raceValue in tableauValues)
                    {
                        if (raceKey.ToLower() == raceValue)
                        {
                            reader.Close();
                            return char.ToUpper(raceValue[0]) + raceValue.Substring(1);
                        }
                    }
                }
            }
            for (int i = 0; i < tableauValues.Length; i++)
            {
                if (tableauValues[i] == "race")
                {
                    string n = null;
                    foreach (char v in tableauValues[i + 1])
                    {
                        if (v == tableauValues[i + 1][0])
                        {
                            n += v.ToString().ToUpper();
                        }
                        else
                        {
                            n += v;
                        }
                    }
                    //RETOUR RACES
                    reader.Close();
                    return n;
                }
            }
            reader.Close();
            return null;
        }
        /// <summary>
        /// Retourne la Valeur qui se trouve après la Valeur nom avec la permiers lettre est en Majuscule
        /// </summary>
        /// <returns></returns>
        public string SearchNom()
        {
            for (int i = 0; i < tableauValues.Length; i++)
            {
                if (tableauValues[i] == "nom")
                {
                    string n = null;
                    foreach (char v in tableauValues[i + 1])
                    {
                        if (v == tableauValues[i + 1][0])
                        {
                            n += v.ToString().ToUpper();
                        }
                        else
                        {
                            n += v;
                        }
                    }
                    return n;
                }
            }
            return null;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string SearchSigne()
        {
            for (int i = 0; i < tableauValues.Length; i++)
            {
                if (tableauValues[i].ToLower() == "signe particulier" || tableauValues[i].ToLower() == "signe")
                {
                    return tableauValues[i + 1];
                }
            }
            return null;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string SearchArchetype()
        {
            for (int i = 0; i < tableauValues.Length; i++)
            {
                if (tableauValues[i].ToLower() == "archetype" || tableauValues[i].ToLower() == "classe")
                {
                    return tableauValues[i + 1];
                }
            }
            return null;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int SearchForce()
        {
            for (int i = 0; i < tableauValues.Length; i++)
            {
                if (tableauValues[i].ToLower() == "force" || tableauValues[i].ToLower() == "for")
                {
                    return int.Parse(tableauValues[i + 1]);
                }
            }
            return 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int SearchEndurance()
        {
            for (int i = 0; i < tableauValues.Length; i++)
            {
                if (tableauValues[i].ToLower() == "endurance" || tableauValues[i].ToLower() == "end")
                {
                    return int.Parse(tableauValues[i + 1]);
                }
            }
            return 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int SearchDexterite()
        {
            for (int i = 0; i < tableauValues.Length; i++)
            {
                if (tableauValues[i].ToLower() == "Dexterite" || tableauValues[i].ToLower() == "dex")
                {
                    return int.Parse(tableauValues[i + 1]);
                }
            }
            return 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int SearchRapidite()
        {
            for (int i = 0; i < tableauValues.Length; i++)
            {
                if (tableauValues[i].ToLower() == "rapidite" || tableauValues[i].ToLower() == "rap")
                {
                    return int.Parse(tableauValues[i + 1]);
                }
            }
            return 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int SearchVolonte()
        {
            for (int i = 0; i < tableauValues.Length; i++)
            {
                if (tableauValues[i].ToLower() == "Volonté" || tableauValues[i].ToLower() == "vol")
                {
                    return int.Parse(tableauValues[i + 1]);
                }
            }
            return 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int SearchPerception()
        {
            for (int i = 0; i < tableauValues.Length; i++)
            {
                if (tableauValues[i].ToLower() == "perception" || tableauValues[i].ToLower() == "per")
                {
                    return int.Parse(tableauValues[i + 1]);
                }
            }
            return 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int SearchIntelligence()
        {
            for (int i = 0; i < tableauValues.Length; i++)
            {
                if (tableauValues[i].ToLower() == "intelligence" || tableauValues[i].ToLower() == "int")
                {
                    return int.Parse(tableauValues[i + 1]);
                }
            }
            return 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int SearchCharisme()
        {
            for (int i = 0; i < tableauValues.Length; i++)
            {
                if (tableauValues[i].ToLower() == "charisme" || tableauValues[i].ToLower() == "cha")
                {
                    return int.Parse(tableauValues[i + 1]);
                }
            }
            return 0;
        }
        /// <summary>
        /// 
        /// </summary>
        public void CreatePersonnage()
        {
            string query = "SELECT TOP 1 Personnages.archetypePersonnage," +
                            " Personnages.nomPersonnage," +
                            " Personnages.signeParticulier," +
                            " Races.nomRace, Races.forRace," +
                            " Personnages.forPersonnage," +
                            " Races.perRace," +
                            " Personnages.perPersonnage," +
                            " Races.endRace," +
                            " Personnages.endPersonnage," +
                            " Races.volRace," +
                            " Personnages.volPersonnage," +
                            " Races.dexRace,\n" +
                            " Personnages.dexPersonnage," +
                            " Races.intRace," +
                            " Personnages.intPersonnage," +
                            " Races.rapRace," +
                            " Personnages.rapPersonnage," +
                            " Races.chaRace," +
                            " Personnages.chaPersonnage," +
                            " Personnages.initiativeBase," +
                            " Races.armureNaturelle," +
                            " Santes.blessureGrave," +
                            " Santes.casesNormal," +
                            " Santes.casesBlesse,\n" +
                            " Santes.casesHandicape," +
                            " Santes.casesIncapacite," +
                            " Races.taille," +
                            " Races.multiplicateurMouvement" +
                            " FROM " +
                            " Personnages INNER JOIN Races ON Personnages.idRace = Races.idRace INNER JOIN " +
                            "Santes ON Personnages.idSante = Santes.idSante INNER JOIN " +
                            "Mails ON Personnages.idMail = Mails.idMail" +
                            " WHERE adressMail = '" + mail + "' ORDER BY idPersonnage DESC;";
            SqlCommand lecture = new SqlCommand(query, con);
            SqlDataReader reader = lecture.ExecuteReader();
            PdfDocument document = new PdfDocument();   //création du document
            PdfDocument FP = PdfReader.Open("C:\\Users\\noah\\Desktop\\D-CRITIQUE\\Feuille de personnage SDC 4.6.pdf", PdfDocumentOpenMode.Import); // import des feuille de Perso
            document.Info.Title = "CREATE TEST TITLE";
            PdfPage page = document.AddPage(FP.Pages[0]);   //Creation de deux pages qui importent les feuilles perso
            PdfPage page2 = document.AddPage(FP.Pages[1]);
            FP.Close();
            XGraphics gfx = XGraphics.FromPdfPage(page);    //Ecriture sur la page 1
            XGraphics gfx2 = XGraphics.FromPdfPage(page2);  //Ecriture sur la page 2
            XFont font = new XFont("Verdana", 10, XFontStyle.Regular);  //Police d'écriture
            XFont fontChiffreRace = new XFont("Verdana", 17, XFontStyle.Bold);  // pour les chiffre
            XFont fontBonus = new XFont("Verdana", 8, XFontStyle.Bold); //pour les bonus
            while (reader.Read())
            {
                // On entre dans le tableau Toutes les colonnes du SELECT
                String[] tableaukey = {
                    reader[0].ToString(),
                    reader[1].ToString(),
                    reader[2].ToString(),
                    reader[3].ToString(),
                    reader[4].ToString(),
                    reader[5].ToString(),
                    reader[6].ToString(),
                    reader[7].ToString(),
                    reader[8].ToString(),
                    reader[9].ToString(),
                    reader[10].ToString(),
                    reader[11].ToString(),
                    reader[12].ToString(),
                    reader[13].ToString(),
                    reader[14].ToString(),
                    reader[15].ToString(),
                    reader[16].ToString(),
                    reader[17].ToString(),
                    reader[18].ToString(),
                    reader[19].ToString(),
                    reader[20].ToString()
                };
                gfx.DrawString(tableaukey[0], font, XBrushes.Black, new XRect(257, 67, page.Width, page.Height), XStringFormats.TopLeft);   //ARCHETYPE
                gfx.DrawString(tableaukey[1], font, XBrushes.Black, new XRect(86, 88, page.Width, page.Height), XStringFormats.TopLeft);    //NOM
                gfx.DrawString(tableaukey[2], font, XBrushes.Black, new XRect(190, 100, page.Width, page.Height), XStringFormats.TopLeft);  // SIGNE PARTICULIERS
                gfx.DrawString(tableaukey[3], font, XBrushes.Black, new XRect(86, 110, page.Width, page.Height), XStringFormats.TopLeft);   //RACES
                gfx.DrawString(tableaukey[4], fontChiffreRace, XBrushes.Black, new XRect(157, 437, page.Width, page.Height), XStringFormats.TopLeft);   //FORCE
                gfx.DrawString(tableaukey[6], fontChiffreRace, XBrushes.Black, new XRect(315, 437, page.Width, page.Height), XStringFormats.TopLeft);   //PERCEPETION
                gfx.DrawString(tableaukey[8], fontChiffreRace, XBrushes.Black, new XRect(157, 475, page.Width, page.Height), XStringFormats.TopLeft);   //ENDURENCE
                gfx.DrawString(tableaukey[10], fontChiffreRace, XBrushes.Black, new XRect(315, 475, page.Width, page.Height), XStringFormats.TopLeft);  //VOLONTE
                gfx.DrawString(tableaukey[12], fontChiffreRace, XBrushes.Black, new XRect(157, 513, page.Width, page.Height), XStringFormats.TopLeft);  //DEXTERITE
                gfx.DrawString(tableaukey[14], fontChiffreRace, XBrushes.Black, new XRect(315, 513, page.Width, page.Height), XStringFormats.TopLeft);  //INTELLIGENCE
                gfx.DrawString(tableaukey[16], fontChiffreRace, XBrushes.Black, new XRect(157, 551, page.Width, page.Height), XStringFormats.TopLeft);  //RAÏDITE
                gfx.DrawString(tableaukey[18], fontChiffreRace, XBrushes.Black, new XRect(315, 551, page.Width, page.Height), XStringFormats.TopLeft);  //CHARISME
                if (int.Parse(tableaukey[5]) >= 1)  //FOR
                {
                    gfx.DrawString("\u2022", fontBonus, XBrushes.Black, new XRect(160, 428, page.Width, page.Height), XStringFormats.TopLeft);
                    if (int.Parse(tableaukey[5]) == 2)
                    {
                        gfx.DrawString("\u2022", fontBonus, XBrushes.Black, new XRect(167, 430, page.Width, page.Height), XStringFormats.TopLeft);
                    }
                }
                if (int.Parse(tableaukey[7]) >= 1)    //PER
                {
                    gfx.DrawString("\u2022", fontBonus, XBrushes.Black, new XRect(318, 428, page.Width, page.Height), XStringFormats.TopLeft);
                    if (int.Parse(tableaukey[7]) == 2)
                    {
                        gfx.DrawString("\u2022", fontBonus, XBrushes.Black, new XRect(325, 430, page.Width, page.Height), XStringFormats.TopLeft);
                    }
                }
                if (int.Parse(tableaukey[9]) >= 1)    //END
                {
                    gfx.DrawString("\u2022", fontBonus, XBrushes.Black, new XRect(160, 466, page.Width, page.Height), XStringFormats.TopLeft);
                    if (int.Parse(tableaukey[9]) == 2)
                    {
                        gfx.DrawString("\u2022", fontBonus, XBrushes.Black, new XRect(167, 468, page.Width, page.Height), XStringFormats.TopLeft);
                    }
                }
                if (int.Parse(tableaukey[11]) >= 1)   //VOL
                {
                    gfx.DrawString("\u2022", fontBonus, XBrushes.Black, new XRect(318, 466, page.Width, page.Height), XStringFormats.TopLeft);
                    if (int.Parse(tableaukey[11]) == 2)
                    {
                        gfx.DrawString("\u2022", fontBonus, XBrushes.Black, new XRect(325, 468, page.Width, page.Height), XStringFormats.TopLeft);
                    }
                }
                if (int.Parse(tableaukey[13]) >= 1)   //DEX
                {
                    gfx.DrawString("\u2022", fontBonus, XBrushes.Black, new XRect(160, 504, page.Width, page.Height), XStringFormats.TopLeft);
                    if (int.Parse(tableaukey[13]) == 2)
                    {
                        gfx.DrawString("\u2022", fontBonus, XBrushes.Black, new XRect(167, 506, page.Width, page.Height), XStringFormats.TopLeft);
                    }
                }
                if (int.Parse(tableaukey[15]) >= 1)   //INT
                {
                    gfx.DrawString("\u2022", fontBonus, XBrushes.Black, new XRect(319, 504, page.Width, page.Height), XStringFormats.TopLeft);
                    if (int.Parse(tableaukey[15]) == 2)
                    {
                        gfx.DrawString("\u2022", fontBonus, XBrushes.Black, new XRect(326, 506, page.Width, page.Height), XStringFormats.TopLeft);
                    }
                }
                if (int.Parse(tableaukey[17]) >= 1)   //RAP
                {
                    gfx.DrawString("\u2022", fontBonus, XBrushes.Black, new XRect(161, 541, page.Width, page.Height), XStringFormats.TopLeft);
                    if (int.Parse(tableaukey[17]) == 2)
                    {
                        gfx.DrawString("\u2022", fontBonus, XBrushes.Black, new XRect(168, 443, page.Width, page.Height), XStringFormats.TopLeft);
                    }
                }
                if (int.Parse(tableaukey[19]) >= 1)   //CHA
                {
                    gfx.DrawString("\u2022", fontBonus, XBrushes.Black, new XRect(318, 541, page.Width, page.Height), XStringFormats.TopLeft);
                    if (int.Parse(tableaukey[19]) == 2)
                    {
                        gfx.DrawString("\u2022", fontBonus, XBrushes.Black, new XRect(325, 543, page.Width, page.Height), XStringFormats.TopLeft);
                    }
                }
                gfx2.DrawString(tableaukey[20], fontChiffreRace, XBrushes.Black, new XRect(375, 40, page2.Width, page2.Height), XStringFormats.TopLeft);
            }
            reader.Close();
            string persoPdf = chemin_mail_a_importer + "\\" + SearchNom() + ".pdf";
            document.Save(persoPdf);
            EnvoiMail(persoPdf);
        }

        public void EnvoiMail(string path)
        {
            MailMessage envoiMail = new MailMessage();
            envoiMail.From = new MailAddress("persodcritique@gmail.com");
            envoiMail.To.Add(mail);
            envoiMail.Subject = "Personnage Créer";
            envoiMail.Body = "Voici Votre Personnage";

            System.Net.Mail.Attachment attachment;
            attachment = new System.Net.Mail.Attachment(path);
            envoiMail.Attachments.Add(attachment);

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential("persodcritique@gmail.com", "Nono19970628");
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(envoiMail);
        }
        public void EnvoieMailError(string message)
        {
            MailMessage envoiMail = new MailMessage();
            envoiMail.From = new MailAddress("persodcritique@gmail.com");
            envoiMail.To.Add(mail);
            envoiMail.Subject = "Création incorrect ";
            envoiMail.Body = message;

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential("persodcritique@gmail.com", "Nono19970628");
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(envoiMail);
        }
    }
}