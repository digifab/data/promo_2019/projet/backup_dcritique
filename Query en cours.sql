SELECT SUM(nbPersonnage) AS nbPersonnage, adressMail FROM DataVIz GROUP BY adressMail;
SELECT SUM(nbPersonnage) AS nbPersonnage, nomUnivers FROM DataVIz GROUP BY nomUnivers;
SELECT SUM(nbRace) AS nbRace, nomUnivers FROM DataVIz GROUP BY nomUnivers;
SELECT COUNT(nomRace) AS nbRace, nomUnivers FROM Races JOIN Univers ON Races.idUnivers = Univers.idUnivers WHERE nomUnivers = 'Warcraft' GROUP BY nomUnivers;
--INSERT INTO DataVIz SELECT COUNT(nomPersonnage) AS nbPersonnage, adressMail,nomUnivers,COUNT(nomRace) AS nbRace, CURRENT_TIMESTAMP FROM Personnages JOIN Mails ON Personnages.idMail = Mails.idMail JOIN Races ON Personnages.idRace = Races.idRace JOIN Univers ON Races.idUnivers = Univers.idUnivers GROUP BY adressMail, nomUnivers;