/*==============================================================*/
/* Nom de SGBD :  Microsoft SQL Server 2016                     */
/* Date de cr�ation :  12/11/2019 16:50:09                      */
/*==============================================================*/


if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ARMES') and o.name = 'FK_ARMES_DANS_EQUI_EQUIPEME')
alter table ARMES
   drop constraint FK_ARMES_DANS_EQUI_EQUIPEME
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ATTRIBUTIONSNOTEARME') and o.name = 'FK_ATTRIBUT_ATTRIBUTI_ARMES')
alter table ATTRIBUTIONSNOTEARME
   drop constraint FK_ATTRIBUT_ATTRIBUTI_ARMES
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('ATTRIBUTIONSNOTEARME') and o.name = 'FK_ATTRIBUT_ATTRIBUTI_NOTESARM')
alter table ATTRIBUTIONSNOTEARME
   drop constraint FK_ATTRIBUT_ATTRIBUTI_NOTESARM
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('COMPETENCES') and o.name = 'FK_COMPETEN_LIEE_DCRITIQU')
alter table COMPETENCES
   drop constraint FK_COMPETEN_LIEE_DCRITIQU
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('COMPETENCESINSTINCTIVES') and o.name = 'FK_COMPETEN_COMPETENC_RACES')
alter table COMPETENCESINSTINCTIVES
   drop constraint FK_COMPETEN_COMPETENC_RACES
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('COMPETENCESINSTINCTIVES') and o.name = 'FK_COMPETEN_COMPETENC_COMPETENR')
alter table COMPETENCESINSTINCTIVES
   drop constraint FK_COMPETEN_COMPETENC_COMPETENR
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('COMPETENCESPERSONNAGE') and o.name = 'FK_COMPETEN_COMPETENC_PERSONNA')
alter table COMPETENCESPERSONNAGE
   drop constraint FK_COMPETEN_COMPETENC_PERSONNA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('COMPETENCESPERSONNAGE') and o.name = 'FK_COMPETEN_COMPETENC_COMPETEN')
alter table COMPETENCESPERSONNAGE
   drop constraint FK_COMPETEN_COMPETENC_COMPETEN
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('COMPETENCEUNIVERS') and o.name = 'FK_COMPETEN_COMPETENC_UNIVERS')
alter table COMPETENCEUNIVERS
   drop constraint FK_COMPETEN_COMPETENC_UNIVERS
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('COMPETENCEUNIVERS') and o.name = 'FK_COMPETEN_COMPETENC_COMPETENU')
alter table COMPETENCEUNIVERS
   drop constraint FK_COMPETEN_COMPETENC_COMPETENU
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('EQUIPEMENTS') and o.name = 'FK_EQUIPEME_POSSEDE_PERSONNA')
alter table EQUIPEMENTS
   drop constraint FK_EQUIPEME_POSSEDE_PERSONNA
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('PERSONNAGES') and o.name = 'FK_PERSONNA_EST_RACES')
alter table PERSONNAGES
   drop constraint FK_PERSONNA_EST_RACES
go

if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('RACES') and o.name = 'FK_RACES_CARACTERI_MULTIPLI')
alter table RACES
   drop constraint FK_RACES_CARACTERI_MULTIPLI
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('ARMES')
            and   name  = 'DANS_EQUIPEMENT_FK'
            and   indid > 0
            and   indid < 255)
   drop index ARMES.DANS_EQUIPEMENT_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ARMES')
            and   type = 'U')
   drop table ARMES
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('ATTRIBUTIONSNOTEARME')
            and   name  = 'ATTRIBUTIONSNOTEARME2_FK'
            and   indid > 0
            and   indid < 255)
   drop index ATTRIBUTIONSNOTEARME.ATTRIBUTIONSNOTEARME2_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('ATTRIBUTIONSNOTEARME')
            and   type = 'U')
   drop table ATTRIBUTIONSNOTEARME
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('COMPETENCES')
            and   name  = 'LIEE_FK'
            and   indid > 0
            and   indid < 255)
   drop index COMPETENCES.LIEE_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('COMPETENCES')
            and   type = 'U')
   drop table COMPETENCES
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('COMPETENCESINSTINCTIVES')
            and   name  = 'COMPETENCESINSTINCTIVES2_FK'
            and   indid > 0
            and   indid < 255)
   drop index COMPETENCESINSTINCTIVES.COMPETENCESINSTINCTIVES2_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('COMPETENCESINSTINCTIVES')
            and   type = 'U')
   drop table COMPETENCESINSTINCTIVES
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('COMPETENCESPERSONNAGE')
            and   name  = 'COMPETENCESPERSONNAGE_FK'
            and   indid > 0
            and   indid < 255)
   drop index COMPETENCESPERSONNAGE.COMPETENCESPERSONNAGE_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('COMPETENCESPERSONNAGE')
            and   type = 'U')
   drop table COMPETENCESPERSONNAGE
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('COMPETENCEUNIVERS')
            and   name  = 'COMPETENCEUNIVERS2_FK'
            and   indid > 0
            and   indid < 255)
   drop index COMPETENCEUNIVERS.COMPETENCEUNIVERS2_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('COMPETENCEUNIVERS')
            and   type = 'U')
   drop table COMPETENCEUNIVERS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('DCRITIQUES')
            and   type = 'U')
   drop table DCRITIQUES
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('EQUIPEMENTS')
            and   name  = 'POSSEDE_FK'
            and   indid > 0
            and   indid < 255)
   drop index EQUIPEMENTS.POSSEDE_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('EQUIPEMENTS')
            and   type = 'U')
   drop table EQUIPEMENTS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('MULTIPLICATEURMOUVEMENT')
            and   type = 'U')
   drop table MULTIPLICATEURMOUVEMENT
go

if exists (select 1
            from  sysobjects
           where  id = object_id('NOTESARMES')
            and   type = 'U')
   drop table NOTESARMES
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('PERSONNAGES')
            and   name  = 'EST_FK'
            and   indid > 0
            and   indid < 255)
   drop index PERSONNAGES.EST_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('PERSONNAGES')
            and   type = 'U')
   drop table PERSONNAGES
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('RACES')
            and   name  = 'CARACTERISE_FK'
            and   indid > 0
            and   indid < 255)
   drop index RACES.CARACTERISE_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('RACES')
            and   type = 'U')
   drop table RACES
go

if exists (select 1
            from  sysobjects
           where  id = object_id('UNIVERS')
            and   type = 'U')
   drop table UNIVERS
go

/*==============================================================*/
/* Table : ARMES                                                */
/*==============================================================*/
create table ARMES (
   IDARME               int                  not null,
   IDEQUIPEMENT         int                  not null,
   DEGATSARME           int                  not null,
   FORCEARME            int                  null,
   TYPEDEGATS           char(1)              not null,
   POIDSMAIN            int                  null,
   POIDSMAINS           int                  null,
   SOLIDITE             int                  null,
   STRUCTUREVIE         int                  null,
   SCOREPARADEPRINCIPAL int                  null,
   SCOREPARADESECONDAIRE int                  null,
   SCOREPLANQUE         int                  null,
   ACTIONRECHARGE       int                  null,
   CHARGEUR             int                  null,
   COURTEDISTANCE       int                  null,
   MOYENNEDISTANCE      int                  null,
   LONGUEDISTANCE       int                  null,
   constraint PK_ARMES primary key (IDARME)
)
go

/*==============================================================*/
/* Index : DANS_EQUIPEMENT_FK                                   */
/*==============================================================*/




create nonclustered index DANS_EQUIPEMENT_FK on ARMES (IDEQUIPEMENT ASC)
go

/*==============================================================*/
/* Table : ATTRIBUTIONSNOTEARME                                 */
/*==============================================================*/
create table ATTRIBUTIONSNOTEARME (
   IDNOTEARME           int                  not null,
   IDARME               int                  not null,
   constraint PK_ATTRIBUTIONSNOTEARME primary key (IDNOTEARME, IDARME)
)
go

/*==============================================================*/
/* Index : ATTRIBUTIONSNOTEARME2_FK                             */
/*==============================================================*/




create nonclustered index ATTRIBUTIONSNOTEARME2_FK on ATTRIBUTIONSNOTEARME (IDNOTEARME ASC)
go

/*==============================================================*/
/* Table : COMPETENCES                                          */
/*==============================================================*/
create table COMPETENCES (
   IDCOMPETENCE         int                  not null,
   DCRITIQUE            int                  null,
   NOMCOMPETENCE        char(25)             not null,
   DESCRIPTIONCOMPETENCE text                 not null,
   constraint PK_COMPETENCES primary key (IDCOMPETENCE)
)
go

/*==============================================================*/
/* Index : LIEE_FK                                              */
/*==============================================================*/




create nonclustered index LIEE_FK on COMPETENCES (DCRITIQUE ASC)
go

/*==============================================================*/
/* Table : COMPETENCESINSTINCTIVES                              */
/*==============================================================*/
create table COMPETENCESINSTINCTIVES (
   IDRACE               int                  not null,
   IDCOMPETENCER        int                  not null,
   constraint PK_COMPETENCESINSTINCTIVES primary key (IDRACE, IDCOMPETENCER)
)
go

/*==============================================================*/
/* Index : COMPETENCESINSTINCTIVES2_FK                          */
/*==============================================================*/




create nonclustered index COMPETENCESINSTINCTIVES2_FK on COMPETENCESINSTINCTIVES (IDRACE ASC)
go

/*==============================================================*/
/* Table : COMPETENCESPERSONNAGE                                */
/*==============================================================*/
create table COMPETENCESPERSONNAGE (
   IDCOMPETENCEP        int                  not null,
   IDPERSONNAGE         int                  not null,
   RANGPERSONNAGE       int                  null,
   constraint PK_COMPETENCESPERSONNAGE primary key (IDCOMPETENCEP, IDPERSONNAGE)
)
go

/*==============================================================*/
/* Index : COMPETENCESPERSONNAGE_FK                             */
/*==============================================================*/




create nonclustered index COMPETENCESPERSONNAGE_FK on COMPETENCESPERSONNAGE (IDPERSONNAGE ASC)
go

/*==============================================================*/
/* Table : COMPETENCEUNIVERS                                    */
/*==============================================================*/
create table COMPETENCEUNIVERS (
   IDUNIVERS            int                  not null,
   IDCOMPETENCEU        int                  not null,
   constraint PK_COMPETENCEUNIVERS primary key (IDUNIVERS, IDCOMPETENCEU)
)
go

/*==============================================================*/
/* Index : COMPETENCEUNIVERS2_FK                                */
/*==============================================================*/




create nonclustered index COMPETENCEUNIVERS2_FK on COMPETENCEUNIVERS (IDUNIVERS ASC)
go

/*==============================================================*/
/* Table : DCRITIQUES                                           */
/*==============================================================*/
create table DCRITIQUES (
   DCRITIQUE            int                  not null,
   constraint PK_DCRITIQUES primary key (DCRITIQUE)
)
go

/*==============================================================*/
/* Table : EQUIPEMENTS                                          */
/*==============================================================*/
create table EQUIPEMENTS (
   IDEQUIPEMENT         int                  not null,
   IDPERSONNAGE         int                  not null,
   NOMEQUIPEMENT        char(35)             not null,
   DESCRIPTIONEQUIPEMENT text                 null,
   COUTEQUIPEMENT       float                not null,
   constraint PK_EQUIPEMENTS primary key (IDEQUIPEMENT)
)
go

/*==============================================================*/
/* Index : POSSEDE_FK                                           */
/*==============================================================*/




create nonclustered index POSSEDE_FK on EQUIPEMENTS (IDPERSONNAGE ASC)
go

/*==============================================================*/
/* Table : MULTIPLICATEURMOUVEMENT                              */
/*==============================================================*/
create table MULTIPLICATEURMOUVEMENT (
   MULTIPLICATERUMOUVEMENT float                not null,
   EXEMPLEMOUVEMENT     text                 not null,
   constraint PK_MULTIPLICATEURMOUVEMENT primary key (MULTIPLICATERUMOUVEMENT)
)
go

/*==============================================================*/
/* Table : NOTESARMES                                           */
/*==============================================================*/
create table NOTESARMES (
   IDNOTEARME           int                  not null,
   SIGLENOTEARME        char(4)              not null,
   NOMNOTEARME          char(25)             not null,
   DESCRIPTIONNOTEARME  text                 not null,
   constraint PK_NOTESARMES primary key (IDNOTEARME)
)
go

/*==============================================================*/
/* Table : PERSONNAGES                                          */
/*==============================================================*/
create table PERSONNAGES (
   IDPERSONNAGE         int                  not null,
   IDRACE               int                  not null,
   IDCOMPTE             int                  not null,
   NOMPERSONNAGE        char(20)             not null,
   ARCHETYPEPERSONNAGE  char(25)             null,
   SIGNEPARTICULIER     text                 null,
   FORPERSONNAGE        int                  not null,
   ENDPERSONNAGE        int                  not null,
   DEXPERSONNAGE        int                  not null,
   RAPPERSONNAGE        int                  not null,
   VOLPERSONNAGE        int                  not null,
   INTPERSONNAGE        int                  not null,
   CHAPERSONNAGE        int                  not null,
   ARMURECOMPLETE       int                  not null,
   BLESSUREGRAVE        int                  not null,
   CASENORMAL           int                  not null,
   CASEBLESSE           int                  not null,
   CASEHANDICAPE        int                  not null,
   CASEINCAPACITE       int                  not null,
   PFPERDU              int                  null,
   PVPERDU              int                  null,
   PLACEMENTCOMBAT      int                  not null,
   NORMALCOMBAT         int                  not null,
   SPRINTCOMBAT         int                  not null,
   DATECREATIONPERSO    timestamp            not null,
   constraint PK_PERSONNAGES primary key (IDPERSONNAGE)
)
go

/*==============================================================*/
/* Index : EST_FK                                               */
/*==============================================================*/




create nonclustered index EST_FK on PERSONNAGES (IDRACE ASC)
go

/*==============================================================*/
/* Table : RACES                                                */
/*==============================================================*/
create table RACES (
   IDRACE               int                  not null,
   MULTIPLICATERUMOUVEMENT float                not null,
   NOMRACE              varchar(15)          not null,
   FORRACE              int                  not null,
   ENDRACE              int                  not null,
   DEXRACE              int                  not null,
   RAPRACE              int                  not null,
   PERRACE              int                  not null,
   VOLRACE              int                  not null,
   INTRACE              int                  not null,
   CHARACE              int                  not null,
   TAILLE               int                  not null,
   ARMURENATURELLE      int                  not null,
   DESCRIPTIONRACE      text                 null,
   constraint PK_RACES primary key (IDRACE)
)
go

/*==============================================================*/
/* Index : CARACTERISE_FK                                       */
/*==============================================================*/




create nonclustered index CARACTERISE_FK on RACES (MULTIPLICATERUMOUVEMENT ASC)
go

/*==============================================================*/
/* Table : UNIVERS                                              */
/*==============================================================*/
create table UNIVERS (
   IDUNIVERS            int                  not null,
   DESCRIPTIONUNIVERS   text                 null,
   NOMUNIVERS           varchar(15)          not null,
   constraint PK_UNIVERS primary key (IDUNIVERS)
)
go

alter table ARMES
   add constraint FK_ARMES_DANS_EQUI_EQUIPEME foreign key (IDEQUIPEMENT)
      references EQUIPEMENTS (IDEQUIPEMENT)
go

alter table ATTRIBUTIONSNOTEARME
   add constraint FK_ATTRIBUT_ATTRIBUTI_ARMES foreign key (IDARME)
      references ARMES (IDARME)
go

alter table ATTRIBUTIONSNOTEARME
   add constraint FK_ATTRIBUT_ATTRIBUTI_NOTESARM foreign key (IDNOTEARME)
      references NOTESARMES (IDNOTEARME)
go

alter table COMPETENCES
   add constraint FK_COMPETEN_LIEE_DCRITIQU foreign key (DCRITIQUE)
      references DCRITIQUES (DCRITIQUE)
go

alter table COMPETENCESINSTINCTIVES
   add constraint FK_COMPETEN_COMPETENC_RACES foreign key (IDRACE)
      references RACES (IDRACE)
go

alter table COMPETENCESINSTINCTIVES
   add constraint FK_COMPETEN_COMPETENC_COMPETENR foreign key (IDCOMPETENCER)
      references COMPETENCES (IDCOMPETENCE)
go

alter table COMPETENCESPERSONNAGE
   add constraint FK_COMPETEN_COMPETENC_PERSONNA foreign key (IDPERSONNAGE)
      references PERSONNAGES (IDPERSONNAGE)
go

alter table COMPETENCESPERSONNAGE
   add constraint FK_COMPETEN_COMPETENC_COMPETEN foreign key (IDCOMPETENCEP)
      references COMPETENCES (IDCOMPETENCE)
go

alter table COMPETENCEUNIVERS
   add constraint FK_COMPETEN_COMPETENC_UNIVERS foreign key (IDUNIVERS)
      references UNIVERS (IDUNIVERS)
go

alter table COMPETENCEUNIVERS
   add constraint FK_COMPETEN_COMPETENC_COMPETENU foreign key (IDCOMPETENCEU)
      references COMPETENCES (IDCOMPETENCE)
go

alter table EQUIPEMENTS
   add constraint FK_EQUIPEME_POSSEDE_PERSONNA foreign key (IDPERSONNAGE)
      references PERSONNAGES (IDPERSONNAGE)
go

alter table PERSONNAGES
   add constraint FK_PERSONNA_EST_RACES foreign key (IDRACE)
      references RACES (IDRACE)
go

alter table RACES
   add constraint FK_RACES_CARACTERI_MULTIPLI foreign key (MULTIPLICATERUMOUVEMENT)
      references MULTIPLICATEURMOUVEMENT (MULTIPLICATERUMOUVEMENT)
go

